package com.example.telegram_bot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;


import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Log
public class TelegrammCity  {
    private Long id;
    private String city;
    private String description;
}
